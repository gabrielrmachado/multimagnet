Experiment 1/1
Team CM 
[[ 376 1624]
 [   1 1999]]
Unique CM 
[[ 485 1515]
 [   4 1996]]

Confusion Matrices' metrics:
Team ACC: 59.38%, VPP: 99.95%, VPN: 18.80%, AUC: 0.594, F1: 0.711
Unique ACC: 62.02%, VPP: 99.80%, VPN: 24.25%, AUC: 0.62, F1: 0.724

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 3, drop_rate = 0.001, tau = RE

Team Statistics: ACC Mean: 59.38%, Positive Mean: 99.95%, Negative Mean: 18.80%, AUC mean: 0.594, F1 Mean: 0.711
Unique Statistics: ACC Mean: 62.02%, Positive Mean: 99.80%, Negative Mean: 24.25%, AUC mean: 0.62, F1 Mean: 0.724
------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1486  514]
 [  54 1946]]
Unique CM 
[[ 406 1594]
 [   2 1998]]

Confusion Matrices' metrics:
Team ACC: 85.80%, VPP: 97.30%, VPN: 74.30%, AUC: 0.858, F1: 0.873
Unique ACC: 60.10%, VPP: 99.90%, VPN: 20.30%, AUC: 0.601, F1: 0.715

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 3, drop_rate = 0.001, tau = minRE

Team Statistics: ACC Mean: 85.80%, Positive Mean: 97.30%, Negative Mean: 74.30%, AUC mean: 0.858, F1 Mean: 0.873
Unique Statistics: ACC Mean: 60.10%, Positive Mean: 99.90%, Negative Mean: 20.30%, AUC mean: 0.601, F1 Mean: 0.715
---------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[ 895 1105]
 [  10 1990]]
Unique CM 
[[1315  685]
 [  11 1989]]

Confusion Matrices' metrics:
Team ACC: 72.12%, VPP: 99.50%, VPN: 44.75%, AUC: 0.721, F1: 0.781
Unique ACC: 82.60%, VPP: 99.45%, VPN: 65.75%, AUC: 0.826, F1: 0.851

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 3, drop_rate = 0.01, tau = RE

Team Statistics: ACC Mean: 72.12%, Positive Mean: 99.50%, Negative Mean: 44.75%, AUC mean: 0.721, F1 Mean: 0.781
Unique Statistics: ACC Mean: 82.60%, Positive Mean: 99.45%, Negative Mean: 65.75%, AUC mean: 0.826, F1 Mean: 0.851
-----------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1904   96]
 [ 740 1260]]
Unique CM 
[[ 894 1106]
 [  14 1986]]

Confusion Matrices' metrics:
Team ACC: 79.10%, VPP: 63.00%, VPN: 95.20%, AUC: 0.791, F1: 0.751
Unique ACC: 72.00%, VPP: 99.30%, VPN: 44.70%, AUC: 0.72, F1: 0.78

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 3, drop_rate = 0.01, tau = minRE

Team Statistics: ACC Mean: 79.10%, Positive Mean: 63.00%, Negative Mean: 95.20%, AUC mean: 0.791, F1 Mean: 0.751
Unique Statistics: ACC Mean: 72.00%, Positive Mean: 99.30%, Negative Mean: 44.70%, AUC mean: 0.72, F1 Mean: 0.78
--------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1673  327]
 [  81 1919]]
Unique CM 
[[1755  245]
 [  99 1901]]

Confusion Matrices' metrics:
Team ACC: 89.80%, VPP: 95.95%, VPN: 83.65%, AUC: 0.898, F1: 0.904
Unique ACC: 91.40%, VPP: 95.05%, VPN: 87.75%, AUC: 0.914, F1: 0.917

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 3, drop_rate = 0.05, tau = RE

Team Statistics: ACC Mean: 89.80%, Positive Mean: 95.95%, Negative Mean: 83.65%, AUC mean: 0.898, F1 Mean: 0.904
Unique Statistics: ACC Mean: 91.40%, Positive Mean: 95.05%, Negative Mean: 87.75%, AUC mean: 0.914, F1 Mean: 0.917
-----------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1804  196]
 [ 350 1650]]
Unique CM 
[[1654  346]
 [  89 1911]]

Confusion Matrices' metrics:
Team ACC: 86.35%, VPP: 82.50%, VPN: 90.20%, AUC: 0.864, F1: 0.858
Unique ACC: 89.12%, VPP: 95.55%, VPN: 82.70%, AUC: 0.891, F1: 0.898

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 3, drop_rate = 0.05, tau = minRE

Team Statistics: ACC Mean: 86.35%, Positive Mean: 82.50%, Negative Mean: 90.20%, AUC mean: 0.864, F1 Mean: 0.858
Unique Statistics: ACC Mean: 89.12%, Positive Mean: 95.55%, Negative Mean: 82.70%, AUC mean: 0.891, F1 Mean: 0.898
--------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[ 415 1585]
 [   2 1998]]
Unique CM 
[[ 787 1213]
 [   7 1993]]

Confusion Matrices' metrics:
Team ACC: 60.32%, VPP: 99.90%, VPN: 20.75%, AUC: 0.603, F1: 0.716
Unique ACC: 69.50%, VPP: 99.65%, VPN: 39.35%, AUC: 0.695, F1: 0.766

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 5, drop_rate = 0.001, tau = RE

Team Statistics: ACC Mean: 60.32%, Positive Mean: 99.90%, Negative Mean: 20.75%, AUC mean: 0.603, F1 Mean: 0.716
Unique Statistics: ACC Mean: 69.50%, Positive Mean: 99.65%, Negative Mean: 39.35%, AUC mean: 0.695, F1 Mean: 0.766
------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1564  436]
 [  48 1952]]
Unique CM 
[[ 584 1416]
 [   4 1996]]

Confusion Matrices' metrics:
Team ACC: 87.90%, VPP: 97.60%, VPN: 78.20%, AUC: 0.879, F1: 0.89
Unique ACC: 64.50%, VPP: 99.80%, VPN: 29.20%, AUC: 0.645, F1: 0.738

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 5, drop_rate = 0.001, tau = minRE

Team Statistics: ACC Mean: 87.90%, Positive Mean: 97.60%, Negative Mean: 78.20%, AUC mean: 0.879, F1 Mean: 0.89
Unique Statistics: ACC Mean: 64.50%, Positive Mean: 99.80%, Negative Mean: 29.20%, AUC mean: 0.645, F1 Mean: 0.738
---------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[ 926 1074]
 [  13 1987]]
Unique CM 
[[1567  433]
 [  14 1986]]

Confusion Matrices' metrics:
Team ACC: 72.82%, VPP: 99.35%, VPN: 46.30%, AUC: 0.728, F1: 0.785
Unique ACC: 88.83%, VPP: 99.30%, VPN: 78.35%, AUC: 0.888, F1: 0.899

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 5, drop_rate = 0.01, tau = RE

Team Statistics: ACC Mean: 72.82%, Positive Mean: 99.35%, Negative Mean: 46.30%, AUC mean: 0.728, F1 Mean: 0.785
Unique Statistics: ACC Mean: 88.83%, Positive Mean: 99.30%, Negative Mean: 78.35%, AUC mean: 0.888, F1 Mean: 0.899
-----------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1613  387]
 [ 135 1865]]
Unique CM 
[[1567  433]
 [  24 1976]]

Confusion Matrices' metrics:
Team ACC: 86.95%, VPP: 93.25%, VPN: 80.65%, AUC: 0.87, F1: 0.877
Unique ACC: 88.58%, VPP: 98.80%, VPN: 78.35%, AUC: 0.886, F1: 0.896

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 5, drop_rate = 0.01, tau = minRE

Team Statistics: ACC Mean: 86.95%, Positive Mean: 93.25%, Negative Mean: 80.65%, AUC mean: 0.87, F1 Mean: 0.877
Unique Statistics: ACC Mean: 88.58%, Positive Mean: 98.80%, Negative Mean: 78.35%, AUC mean: 0.886, F1 Mean: 0.896
--------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1596  404]
 [  86 1914]]
Unique CM 
[[1654  346]
 [  87 1913]]

Confusion Matrices' metrics:
Team ACC: 87.75%, VPP: 95.70%, VPN: 79.80%, AUC: 0.877, F1: 0.887
Unique ACC: 89.18%, VPP: 95.65%, VPN: 82.70%, AUC: 0.892, F1: 0.898

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 5, drop_rate = 0.05, tau = RE

Team Statistics: ACC Mean: 87.75%, Positive Mean: 95.70%, Negative Mean: 79.80%, AUC mean: 0.877, F1 Mean: 0.887
Unique Statistics: ACC Mean: 89.18%, Positive Mean: 95.65%, Negative Mean: 82.70%, AUC mean: 0.892, F1 Mean: 0.898
-----------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1898  102]
 [ 604 1396]]
Unique CM 
[[1545  455]
 [  84 1916]]

Confusion Matrices' metrics:
Team ACC: 82.35%, VPP: 69.80%, VPN: 94.90%, AUC: 0.823, F1: 0.798
Unique ACC: 86.52%, VPP: 95.80%, VPN: 77.25%, AUC: 0.865, F1: 0.877

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 5, drop_rate = 0.05, tau = minRE

Team Statistics: ACC Mean: 82.35%, Positive Mean: 69.80%, Negative Mean: 94.90%, AUC mean: 0.823, F1 Mean: 0.798
Unique Statistics: ACC Mean: 86.52%, Positive Mean: 95.80%, Negative Mean: 77.25%, AUC mean: 0.865, F1 Mean: 0.877
--------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[ 521 1479]
 [   2 1998]]
Unique CM 
[[  11 1989]
 [   1 1999]]

Confusion Matrices' metrics:
Team ACC: 62.98%, VPP: 99.90%, VPN: 26.05%, AUC: 0.63, F1: 0.73
Unique ACC: 50.25%, VPP: 99.95%, VPN: 0.55%, AUC: 0.503, F1: 0.668

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 7, drop_rate = 0.001, tau = RE

Team Statistics: ACC Mean: 62.98%, Positive Mean: 99.90%, Negative Mean: 26.05%, AUC mean: 0.63, F1 Mean: 0.73
Unique Statistics: ACC Mean: 50.25%, Positive Mean: 99.95%, Negative Mean: 0.55%, AUC mean: 0.503, F1 Mean: 0.668
------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1544  456]
 [  57 1943]]
Unique CM 
[[ 787 1213]
 [   2 1998]]

Confusion Matrices' metrics:
Team ACC: 87.17%, VPP: 97.15%, VPN: 77.20%, AUC: 0.872, F1: 0.883
Unique ACC: 69.62%, VPP: 99.90%, VPN: 39.35%, AUC: 0.696, F1: 0.767

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 7, drop_rate = 0.001, tau = minRE

Team Statistics: ACC Mean: 87.17%, Positive Mean: 97.15%, Negative Mean: 77.20%, AUC mean: 0.872, F1 Mean: 0.883
Unique Statistics: ACC Mean: 69.62%, Positive Mean: 99.90%, Negative Mean: 39.35%, AUC mean: 0.696, F1 Mean: 0.767
---------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1023  977]
 [  16 1984]]
Unique CM 
[[1476  524]
 [  12 1988]]

Confusion Matrices' metrics:
Team ACC: 75.17%, VPP: 99.20%, VPN: 51.15%, AUC: 0.752, F1: 0.8
Unique ACC: 86.60%, VPP: 99.40%, VPN: 73.80%, AUC: 0.866, F1: 0.881

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 7, drop_rate = 0.01, tau = RE

Team Statistics: ACC Mean: 75.17%, Positive Mean: 99.20%, Negative Mean: 51.15%, AUC mean: 0.752, F1 Mean: 0.8
Unique Statistics: ACC Mean: 86.60%, Positive Mean: 99.40%, Negative Mean: 73.80%, AUC mean: 0.866, F1 Mean: 0.881
-----------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1832  168]
 [ 320 1680]]
Unique CM 
[[1315  685]
 [  14 1986]]

Confusion Matrices' metrics:
Team ACC: 87.80%, VPP: 84.00%, VPN: 91.60%, AUC: 0.878, F1: 0.873
Unique ACC: 82.53%, VPP: 99.30%, VPN: 65.75%, AUC: 0.825, F1: 0.85

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 7, drop_rate = 0.01, tau = minRE

Team Statistics: ACC Mean: 87.80%, Positive Mean: 84.00%, Negative Mean: 91.60%, AUC mean: 0.878, F1 Mean: 0.873
Unique Statistics: ACC Mean: 82.53%, Positive Mean: 99.30%, Negative Mean: 65.75%, AUC mean: 0.825, F1 Mean: 0.85
--------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1515  485]
 [  81 1919]]
Unique CM 
[[1670  330]
 [  88 1912]]

Confusion Matrices' metrics:
Team ACC: 85.85%, VPP: 95.95%, VPN: 75.75%, AUC: 0.859, F1: 0.871
Unique ACC: 89.55%, VPP: 95.60%, VPN: 83.50%, AUC: 0.895, F1: 0.901

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 7, drop_rate = 0.05, tau = RE

Team Statistics: ACC Mean: 85.85%, Positive Mean: 95.95%, Negative Mean: 75.75%, AUC mean: 0.859, F1 Mean: 0.871
Unique Statistics: ACC Mean: 89.55%, Positive Mean: 95.60%, Negative Mean: 83.50%, AUC mean: 0.895, F1 Mean: 0.901
-----------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1931   69]
 [ 671 1329]]
Unique CM 
[[1755  245]
 [ 107 1893]]

Confusion Matrices' metrics:
Team ACC: 81.50%, VPP: 66.45%, VPN: 96.55%, AUC: 0.815, F1: 0.782
Unique ACC: 91.20%, VPP: 94.65%, VPN: 87.75%, AUC: 0.912, F1: 0.915

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 7, drop_rate = 0.05, tau = minRE

Team Statistics: ACC Mean: 81.50%, Positive Mean: 66.45%, Negative Mean: 96.55%, AUC mean: 0.815, F1 Mean: 0.782
Unique Statistics: ACC Mean: 91.20%, Positive Mean: 94.65%, Negative Mean: 87.75%, AUC mean: 0.912, F1 Mean: 0.915
--------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[ 599 1401]
 [   1 1999]]
Unique CM 
[[1184  816]
 [   0 2000]]

Confusion Matrices' metrics:
Team ACC: 64.95%, VPP: 99.95%, VPN: 29.95%, AUC: 0.649, F1: 0.74
Unique ACC: 79.60%, VPP: 100.00%, VPN: 59.20%, AUC: 0.796, F1: 0.831

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 9, drop_rate = 0.001, tau = RE

Team Statistics: ACC Mean: 64.95%, Positive Mean: 99.95%, Negative Mean: 29.95%, AUC mean: 0.649, F1 Mean: 0.74
Unique Statistics: ACC Mean: 79.60%, Positive Mean: 100.00%, Negative Mean: 59.20%, AUC mean: 0.796, F1 Mean: 0.831
------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1663  337]
 [ 145 1855]]
Unique CM 
[[  11 1989]
 [   0 2000]]

Confusion Matrices' metrics:
Team ACC: 87.95%, VPP: 92.75%, VPN: 83.15%, AUC: 0.88, F1: 0.885
Unique ACC: 50.28%, VPP: 100.00%, VPN: 0.55%, AUC: 0.503, F1: 0.668

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 9, drop_rate = 0.001, tau = minRE

Team Statistics: ACC Mean: 87.95%, Positive Mean: 92.75%, Negative Mean: 83.15%, AUC mean: 0.88, F1 Mean: 0.885
Unique Statistics: ACC Mean: 50.28%, Positive Mean: 100.00%, Negative Mean: 0.55%, AUC mean: 0.503, F1 Mean: 0.668
---------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1135  865]
 [  14 1986]]
Unique CM 
[[1476  524]
 [  24 1976]]

Confusion Matrices' metrics:
Team ACC: 78.03%, VPP: 99.30%, VPN: 56.75%, AUC: 0.78, F1: 0.819
Unique ACC: 86.30%, VPP: 98.80%, VPN: 73.80%, AUC: 0.863, F1: 0.878

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 9, drop_rate = 0.01, tau = RE

Team Statistics: ACC Mean: 78.03%, Positive Mean: 99.30%, Negative Mean: 56.75%, AUC mean: 0.78, F1 Mean: 0.819
Unique Statistics: ACC Mean: 86.30%, Positive Mean: 98.80%, Negative Mean: 73.80%, AUC mean: 0.863, F1 Mean: 0.878
-----------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1883  117]
 [ 532 1468]]
Unique CM 
[[ 894 1106]
 [  18 1982]]

Confusion Matrices' metrics:
Team ACC: 83.78%, VPP: 73.40%, VPN: 94.15%, AUC: 0.838, F1: 0.819
Unique ACC: 71.90%, VPP: 99.10%, VPN: 44.70%, AUC: 0.719, F1: 0.779

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 9, drop_rate = 0.01, tau = minRE

Team Statistics: ACC Mean: 83.78%, Positive Mean: 73.40%, Negative Mean: 94.15%, AUC mean: 0.838, F1 Mean: 0.819
Unique Statistics: ACC Mean: 71.90%, Positive Mean: 99.10%, Negative Mean: 44.70%, AUC mean: 0.719, F1 Mean: 0.779
--------------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1597  403]
 [  86 1914]]
Unique CM 
[[1544  456]
 [  90 1910]]

Confusion Matrices' metrics:
Team ACC: 87.78%, VPP: 95.70%, VPN: 79.85%, AUC: 0.878, F1: 0.887
Unique ACC: 86.35%, VPP: 95.50%, VPN: 77.20%, AUC: 0.864, F1: 0.875

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 9, drop_rate = 0.05, tau = RE

Team Statistics: ACC Mean: 87.78%, Positive Mean: 95.70%, Negative Mean: 79.85%, AUC mean: 0.878, F1 Mean: 0.887
Unique Statistics: ACC Mean: 86.35%, Positive Mean: 95.50%, Negative Mean: 77.20%, AUC mean: 0.864, F1 Mean: 0.875
-----------------------------------------------------------------------------------------------------------------------------------------

Experiment 1/1
Team CM 
[[1938   62]
 [ 699 1301]]
Unique CM 
[[1545  455]
 [  88 1912]]

Confusion Matrices' metrics:
Team ACC: 80.97%, VPP: 65.05%, VPN: 96.90%, AUC: 0.81, F1: 0.774
Unique ACC: 86.42%, VPP: 95.60%, VPN: 77.25%, AUC: 0.864, F1: 0.876

EXPERIMENT 2 TERMINATED. MNIST DATASET: 4000 Input Images 'x', DEEPFOOL Attack, p = 1, reduction models = 9, drop_rate = 0.05, tau = minRE

Team Statistics: ACC Mean: 80.97%, Positive Mean: 65.05%, Negative Mean: 96.90%, AUC mean: 0.81, F1 Mean: 0.774
Unique Statistics: ACC Mean: 86.42%, Positive Mean: 95.60%, Negative Mean: 77.25%, AUC mean: 0.864, F1 Mean: 0.876
--------------------------------------------------------------------------------------------------------------------------------------------


Experiment's elapsed time: 0:16:54.934366